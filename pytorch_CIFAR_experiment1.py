#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 14:33:26 2017

@author: sihao

Experiment 1 for CIFAR10:
    Running a max threshold network on CIFAR10
    Network has two convolutional layers, but chooses between the convolutional 
    output or a max input for each point on first layer based on threshold on 
    max input
    
    max input is a maxpool around area with same size as conv kernel size
    
"""



import torch
import torchvision
import torchvision.transforms as transforms
import experiment_utils as utils
import seaborn as sns
sns.set_style(style='white')
import visdom
import os
from numpy import flipud
import matplotlib.pyplot as plt
vis = visdom.Visdom()

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)), utils.RGB2Grey()])
root = './data'

trainset = torchvision.datasets.CIFAR10(root=root, train=True,
                                        download=True, transform = transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root=root, train=False,
                                       download=True, transform = transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

import matplotlib.pyplot as plt
import numpy as np

# functions to show an image


def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))



# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()
images = torch.unsqueeze(images, dim=1)
# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F





class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.max_pool1 = nn.MaxPool2d(5, stride  = 1)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 12, 5)
        self.fc1 = nn.Linear(12 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10) 
        self.threshold1 = nn.Parameter(torch.randn((4, 6, 28, 28)), requires_grad=True)

    def forward(self, x):
        # Layer 1
        x_1 = F.relu(self.conv1(x))
        x_2 = self.max_pool1(x)
        self.cond = torch.lt(x_2, self.threshold1) # less than operation with broadcastings
        self.cond = self.cond.type('torch.FloatTensor')
        x = self.where(self.cond, x_1, x_2)
        x = self.pool(x)
        
        # Layer 2
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        
        # Layer 3
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        
        # Layer 4
        x = F.relu(self.fc2(x))
        
        # Layer 5
        x = self.fc3(x)
        return x
    
    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
    def where(self, cond, x_1, x_2): 
        return (cond * x_1) + ((1-cond) * x_2)
net = Net()

########################################################################
# 3. Define a Loss function and optimizer
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters())




########################################################################
# 4. Train the network (only if not trained already)
# ^^^^^^^^^^^^^^^^^^^^
saved_params = 'CIFAR10_exp1_model_params'
plot_env = 'CIFAR10_max_thresh_1_conv'

if not os.path.isfile(saved_params):

    for epoch in range(3):  # loop over the dataset multiple times
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels = data
            inputs = torch.unsqueeze(inputs, dim=1)

    
            # wrap them in Variable
            inputs, labels = Variable(inputs), Variable(labels)
    
            # zero the parameter gradients
            optimizer.zero_grad()
    
            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
    
            # print statistics
            running_loss += loss.data[0]
            if i % 500 == 499:    # print every 500 mini-batches
                if i == 499:
                    loss_plot = vis.line(Y=np.array([running_loss/500]), X=np.array([i]), env=plot_env, opts=dict(title='Loss, epoch: %d' %(epoch+1)))
    
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 500))
                vis.updateTrace(Y=np.array([running_loss/500]), X=np.array([i+1]), win=loss_plot, env=plot_env)
                running_loss = 0.0
    
    print('Finished Training')
    print('Saving model')
    torch.save(net.state_dict(),saved_params)


else:
    
    net.load_state_dict(torch.load(saved_params))


########################################################################
# 5. Test the network on the test data
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#

dataiter = iter(testloader)
images, labels = dataiter.next()
images = torch.unsqueeze(images, dim=1)

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))



outputs = net(Variable(images))

########################################################################
# The outputs are energies for the 10 classes.

_, predicted = torch.max(outputs.data, 1)

print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(4)))


########################################################################
# Test on entire dataset

correct = 0
total = 0
for data in testloader:
    images, labels = data
    images = torch.unsqueeze(images, dim=1)
    
    # show images
#    imshow(torchvision.utils.make_grid(images))
    # print labels
#    print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

    outputs = net(Variable(images))
    
    # Show MAX activation location
#    imshow(torchvision.utils.make_grid(torch.unsqueeze(net.cond[i,:,:,:].data, dim = 1)))

    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total))
vis.text('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total), env=plot_env)
########################################################################
# Per class accuracy

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
cond_stack = torch.LongTensor(10, 28, 28).zero_() # 10 classes, cond kernels are 24x24
for count, data in enumerate(testloader):
    images, labels = data
    images = torch.unsqueeze(images, dim=1)
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += c[i]
        class_total[label] += 1
        
        cond_stack[label,:,:] = cond_stack[label,:,:] + torch.sum(net.cond[i,:,:,:].data, dim=0).type(torch.LongTensor)

cond_stack = cond_stack.type(torch.FloatTensor)/(torch.max(cond_stack)) #Four images per mini-batch
for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))
    

########################################################################    
# Visualise the convolutional weights

fig, axarr = plt.subplots(int(np.ceil(net.conv1.weight.squeeze().size()[0]/3)), 3)
[ax.axis('off') for ax in axarr.flat]


for i in range(net.conv1.weight.squeeze().size()[0]):
#    vis.heatmap(net.conv1.weight[i].squeeze().data.numpy(), opts=dict(title='Convolution layer 1: kernel %d' %i), env=plot_env)
    sns.heatmap(net.conv1.weight[i].squeeze().data.numpy(), ax = axarr.flat[i], cbar = 0, cmap = "viridis")

fig.tight_layout()    
fig.savefig('conv_kernels.eps', bbox_inches='tight', pad_inches=0)

for i in range(net.threshold1.size()[0]):
    for j in range(net.threshold1.size()[1]):
        vis.heatmap(net.threshold1.data.numpy()[i,j,:,:], opts=dict(title='Threshold map 1: image %d, kernel %d' %(i,j)), env =plot_env)
        
########################################################################    
# Visualise where MAX is used
fig, axarr = plt.subplots(int(np.ceil(net.conv1.weight.squeeze().size()[0]/4)), 4)
[ax.set_axis_off() for ax in axarr.flat]  

for i in range(net.cond.size()[0]):
    for j in range(net.cond.size()[1]):
            vis.heatmap(net.cond[i,j,:,:].data.numpy(), opts=dict(title='Max mode: image %d, kernel %d' %(i,j)), env=plot_env)

########################################################################    
# Visualise where MAX is used per class
fig, axarr = plt.subplots(int(np.ceil(cond_stack.numpy().shape[0]/3)), 3)
[ax.axis('off') for ax in axarr.flat]

for class_num, image in enumerate(cond_stack.numpy()):
#    vis.heatmap(flipud(image), opts=dict(title='MAX mode locations for class %s' %classes[class_num]), env=plot_env)
    sns.heatmap(cond_stack.numpy()[class_num], ax = axarr.flat[class_num], cbar = 0, cmap = "viridis")
    


fig.tight_layout()    
fig.savefig('max_locations.eps', bbox_inches='tight', pad_inches=0)
