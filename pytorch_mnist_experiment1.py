#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 14:33:26 2017

@author: sihao

Experiment 1:
    Running a standard convolutional neural network on the MNIST dataset
"""



import torch
import torchvision
import torchvision.transforms as transforms
import seaborn as sns
sns.set_style(style='white')
import visdom

vis = visdom.Visdom()

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
root = './data'

trainset = torchvision.datasets.MNIST(root=root, train=True,
                                        download=True, transform = transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.MNIST(root=root, train=False,
                                       download=True, transform = transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = range(10)

import matplotlib.pyplot as plt
import numpy as np

# functions to show an image


def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))



# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 4 * 4, 120) # 16 outputs from 2nd conv layer, each 4 x 4 in size
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, stim):
        stim = self.pool(F.relu(self.conv1(stim)))
        stim = self.pool(F.relu(self.conv2(stim)))
        stim = stim.view(-1, 16 * 4 * 4) # 16 outputs from 2nd conv layer, each 4 x 4 in size
        stim = F.relu(self.fc1(stim))
        stim = F.relu(self.fc2(stim))
        stim = self.fc3(stim)
        return stim
    
    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
    def where(self, cond, x_1, x_2):
        return (cond * x_1) + ((1-cond) * x_2)
net = Net()

########################################################################
# 3. Define a Loss function and optimizer
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

########################################################################
# 4. Train the network
# ^^^^^^^^^^^^^^^^^^^^


for epoch in range(2):  # loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data

        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        

        # print statistics
        running_loss += loss.data[0]
        if i % 500 == 499:    # print every 500 mini-batches
            if (i == 499):
                loss_plot = vis.line(Y=np.array([running_loss/500]), X=np.array([i]), env='standard_convnet', opts=dict(title='Loss, epoch: %d' %(epoch+1)))
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 500))
            vis.updateTrace(Y=np.array([running_loss/500]), X=np.array([i+1]), win=loss_plot, env='standard_convnet')
            running_loss = 0.0

print('Finished Training')

########################################################################
# 5. Test the network on the test data
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


dataiter = iter(testloader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

########################################################################

outputs = net(Variable(images))

########################################################################
# The outputs are energies for the 10 classes.


print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(4)))


########################################################################
# Test on entire test set

correct = 0
total = 0
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total))
vis.text('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total), env='standard_convnet')
########################################################################
# Per class accuracy

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += c[i]
        class_total[label] += 1


for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))
    
    
########################################################################
# Visualise the convolutional weights
    

for i in range(net.conv1.weight.squeeze().size()[0]):
    vis.heatmap(net.conv1.weight[i].squeeze().data.numpy(), opts=dict(title='Convolution layer 1: kernel %d' %i), env='standard_convnet')

