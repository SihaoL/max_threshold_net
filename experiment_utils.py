#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 17:56:53 2017

@author: sihao
"""
import torch

def rgb2grey(tensor):
    """ Notes
        -----
        The weights used in this conversion are calibrated for contemporary
        CRT phosphors::
            Y = 0.2125 R + 0.7154 G + 0.0721 B
    """
    
    out = 0.2125 * tensor[0,:,:] + 0.7154 * tensor[1, :, :] + 0.0721 * tensor[2, :, :]
    torch.unsqueeze(out, dim=0)
    torch.unsqueeze(out, dim=0)


    
    return out
    
class RGB2Grey(object):
    """Convert three channel RGB image to single channel greyscale image.
        

    """
        

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be converted.
        Returns:
            Tensor: Normalized image.
        """
        return rgb2grey(tensor)