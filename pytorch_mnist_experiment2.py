#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 14:33:26 2017

@author: sihao

Experiment 2:
    Running a max threshold network on MNIST
    Network has two convolutional layers, but chooses between the convolutional 
    output or a max input for each point on both layers based on threshold on 
    max input
    
    max input is a maxpool around area with same size as conv kernel size
    
    
"""



import torch
import torchvision
import torchvision.transforms as transforms
import seaborn as sns
sns.set_style(style='white')
import visdom

vis = visdom.Visdom()

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
root = './data'

trainset = torchvision.datasets.MNIST(root=root, train=True,
                                        download=True, transform = transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.MNIST(root=root, train=False,
                                       download=True, transform = transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = range(10)

import matplotlib.pyplot as plt
import numpy as np

# functions to show an image


def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))



# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.max_pool1 = nn.MaxPool2d(5, stride  = 1)
        self.max_pool2 = nn.MaxPool2d(5, stride = 1)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 12, 5)
        self.fc1 = nn.Linear(12 * 4 * 4, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10) 
        self.threshold1 = nn.Parameter(torch.randn((4, 6, 24, 24)), requires_grad=True)
        self.threshold2 = nn.Parameter(torch.randn((4, 12, 8, 8)), requires_grad=True)

    def forward(self, x):
        # Layer 1
        x_1 = F.relu(self.conv1(x))
        x_2 = self.max_pool1(x)
        x_2 = torch.cat((x_2,x_2,x_2,x_2,x_2,x_2), dim = 1)
        cond = x_2 < self.threshold1
        cond = cond.type('torch.FloatTensor')
        x = self.where(cond, x_1, x_2)
        x = self.pool(x)
        
        # Layer 2
        x_1 = F.relu(self.conv2(x))
        x_2 = self.max_pool2(x)
        x_2 = torch.cat((x_2,x_2), dim = 1)
        cond = x_2 < self.threshold2
        cond = cond.type('torch.FloatTensor')
        x = self.where(cond, x_1, x_2)
        x = self.pool(x)
        
        # Layer 3
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        
        # Layer 4
        x = F.relu(self.fc2(x))
        
        # Layer 5
        x = self.fc3(x)
        return x
    
    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
    def where(self, cond, x_1, x_2):
        return (cond * x_1) + ((1-cond) * x_2)
net = Net()

########################################################################
# 3. Define a Loss function and optimizer
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

########################################################################
# 4. Train the network
# ^^^^^^^^^^^^^^^^^^^^

for epoch in range(2):  # loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data

        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()


        # print statistics
        running_loss += loss.data[0]
        if i % 500 == 499:    # print every 500 mini-batches
            if i == 499:
                loss_plot = vis.line(Y=np.array([running_loss/500]), X=np.array([i]),opts=dict(title='Loss, epoch: %d' %(epoch+1)), env ='max_thresh_2_conv')
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 500))
            vis.updateTrace(Y=np.array([running_loss/500]), X=np.array([i+1]), win=loss_plot, env ='max_thresh_2_conv')
            running_loss = 0.0

print('Finished Training')

########################################################################
# 5. Test the network on the test data
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#


dataiter = iter(testloader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

########################################################################

outputs = net(Variable(images))

########################################################################
# The outputs are energies for the 10 classes.

_, predicted = torch.max(outputs.data, 1)

print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(4)))


########################################################################
# Test on entire test set

correct = 0
total = 0
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total))
vis.text('Accuracy of the network on the 10000 test images: %f %%' % (
    100 * correct / total), env='max_thresh_2_conv')
########################################################################
# Per class accuracy

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
for data in testloader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += c[i]
        class_total[label] += 1


for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))
    
    
########################################################################
# Visualise the convolutional weights
    


for i in range(net.conv1.weight.squeeze().size()[0]):
    vis.heatmap(net.conv1.weight[i].squeeze().data.numpy(), opts=dict(title='Convolution layer 1: kernel %d' %i), env ='max_thresh_2_conv')
    
for i in range(net.threshold1.size()[0]):
    for j in range(net.threshold1.size()[1]):
        vis.heatmap(net.threshold1.data.numpy()[i,j,:,:], opts=dict(title='Threshold map 1: image %d, kernel %d' %(i,j)), env ='max_thresh_2_conv')

for i in range(net.threshold2.size()[0]):
    for j in range(net.threshold2.size()[1]):
        vis.heatmap(net.threshold2.data.numpy()[i,j,:,:], opts=dict(title='Threshold map 2: image %d, kernel %d' %(i,j)), env ='max_thresh_2_conv')
